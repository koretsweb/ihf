<?php
/**
 * Created by PhpStorm.
 * User: pc60
 * Date: 23.02.18
 * Time: 16:56
 */

class UserSeeder extends \Illuminate\Database\Seeder
{
    public function run()
    {
        \App\User::create([
            'name' => 'Admin',
            'email' => 'admin@ihf.com',
            'password' => bcrypt('admin'),
            'telegram' => 'dima_korets',
            'phone' => "+380962460035"
        ]);
    }
}