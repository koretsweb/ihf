<?php

namespace App\Console\Commands;

use App\Services\ReadAllObjects;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class RunMonitor extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'runMonitor';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run monitoring action';

    /**
     * @var ReadAllObjects
     */
    protected $service;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->service = app(ReadAllObjects::class);
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        echo chr(27) . "[32m" . "Моніторинг почав роботу" . chr(27) . "[0m" . PHP_EOL;
        while (true)
        {
            $data = $this->service->run();

            if (!empty($data)) {
                echo Carbon::now() . PHP_EOL;
                foreach ($data as $name => $item) {
                    $color = "[32m";
                    if ($item['out_of_range']) {
                        $color = "[31m";
                    }
                    echo chr(27) . $color . $name . ': ' . $item['value'] . $item['units'] . chr(27) . "[0m" . PHP_EOL . PHP_EOL;
                }
            }

            sleep(1);
        }
    }
}
