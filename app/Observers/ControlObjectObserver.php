<?php
/**
 * @author Dima Korets korets.web@gmail.com
 * @Date: 24.02.18
 */

namespace App\Observers;


use App\Models\ControlObject;
use Carbon\Carbon;

class ControlObjectObserver
{
    public function updating(ControlObject $object)
    {
        $object->last_read = Carbon::now();
    }
}