<?php
/**
 * Created by PhpStorm.
 * User: pc60
 * Date: 23.02.18
 * Time: 14:35
 */

namespace App\Observers;


use App\Models\Result;
use App\Notifications\Accident;
use App\User;

class ResultsObserver
{
    public function created(Result $result)
    {
        if ($result->out_of_range) {
            // hardcoded user
            User::first()->notify(new Accident($result));
        }
    }
}