<?php
/**
 * Created by PhpStorm.
 * User: pc60
 * Date: 23.02.18
 * Time: 13:36
 */

namespace App\Repositories;


use App\Models\ControlObject;

class ControlObjectsRepository
{
    protected $model;

    public function __construct(ControlObject $controlObject)
    {
        $this->model = $controlObject;
    }
}