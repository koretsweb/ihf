<?php
/**
 * @author Dima Korets korets.web@gmail.com
 * @Date: 22.02.18
 */

namespace App\Repositories;


use App\Models\Result;

class ResultsRepository
{
    /**
     * @var Result
     */
    protected $model;

    public function __construct(Result $result)
    {
        $this->model = $result;
    }
}