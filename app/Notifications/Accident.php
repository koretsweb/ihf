<?php

namespace App\Notifications;

use App\Models\ControlObject;
use App\Models\Result;
use App\Services\SendSMS;
use App\User;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;

class Accident extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var Result
     */
    protected $result;

    /**
     * Create a new notification instance.
     * @param Result $result
     * @return void
     */
    public function __construct(Result $result)
    {
        $this->result = $result;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  User  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        if ($this->internetConnectionExists()) {
            $via = $this->result->object->notification_channels;
        }
        $via[] = 'database';

        return $via;
    }

    /**
     * @return bool
     */
    protected function internetConnectionExists()
    {
        $connected = @fsockopen("www.example.com", 80);
        //website, port  (try 80 or 443)
        if ($connected){
            $is_conn = true; //action when connected
            fclose($connected);
        }else{
            $is_conn = false; //action in connection failure
        }
        return $is_conn;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return mixed
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line(      Carbon::now() . " були зняті наступні показники: " .
                $this->result->value . ControlObject::getUnitsMap()[$this->result->object->units] .
                " на об'єкті " . $this->result->object->name . " при допустимому діапазоні " .
                $this->result->object->min_value . "-" . $this->result->object->max_value .
                ControlObject::getUnitsMap()[$this->result->object->units])
            ->action('Перейти до центру моніторингу', env('APP_URL'));
    }

    /**
     * @param $notifiable
     * @throws \Twilio\Exceptions\ConfigurationException
     */
    public function toSms($notifiable = null)
    {
        // todo
//        $service = new SendSMS(User::first()->phone, "An accident on " . $this->result->object->name . ' control object!');
//        $service->run();
    }

    public function toTelegram()
    {
        return TelegramMessage::create()
            ->to(324084818) // todo
            ->content("*Спрацювання сигналізації!* \n " .
                Carbon::now() . " були зняті наступні показники: " .
                $this->result->value . ControlObject::getUnitsMap()[$this->result->object->units] .
                " на об'єкті " . $this->result->object->name . " при допустимому діапазоні " .
                $this->result->object->min_value . "-" . $this->result->object->max_value .
                ControlObject::getUnitsMap()[$this->result->object->units]
            )
            ->button('Перейти до центру моніторинга', env('APP_URL'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'message' => $this->result->value . ControlObject::getUnitsMap()[$this->result->object->units] .
                " на об'єкті " . $this->result->object->name,
            'result_id' => $this->result->id,
        ];
    }
}
