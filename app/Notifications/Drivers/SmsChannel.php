<?php
/**
 * @author Dima Korets korets.web@gmail.com
 * @Date: 25.02.18
 */

namespace App\Notifications\Drivers;

use Illuminate\Notifications\Notification;

class SmsChannel
{
    /**
     * @param $notifiable
     * @param Notification $notification
     * @return mixed
     */
    public function send($notifiable, Notification $notification)
    {
        return $notification->toSms();
    }
}