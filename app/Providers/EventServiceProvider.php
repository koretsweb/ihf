<?php

namespace App\Providers;

use App\Models\ControlObject;
use App\Models\Result;
use App\Observers\ControlObjectObserver;
use App\Observers\ResultsObserver;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\Event' => [
            'App\Listeners\EventListener',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        Result::observe(ResultsObserver::class);
        ControlObject::observe(ControlObjectObserver::class);
    }
}
