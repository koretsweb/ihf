<?php
/**
 * @author Dima Korets korets.web@gmail.com
 * @Date: 22.02.18
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * Class Result
 * @package App\Models
 *
 * @property integer $id
 * @property integer $value
 * @property integer $object_id
 * @property string $created_type
 * @property boolean $out_of_range
 * @property ControlObject $object
 */
class Result extends Model
{
    protected $table = 'results';

    protected $fillable = ['value', 'out_of_range'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function object()
    {
        return $this->belongsTo(ControlObject::class);
    }


    public function scopeOutOfRange($query)
    {
        return $query->where('out_of_range', true);
    }
}