<?php
/**
 * @author Dima Korets korets.web@gmail.com
 * @Date: 24.02.18
 */

namespace App\Models;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Report
 * @package App\Models
 *
 * @property integer $id
 * @property string $path
 * @property integer $object_id
 * @property Carbon $start_date
 * @property Carbon $end_date
 * @property string $type
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property ControlObject $object
 */
class Report extends Model
{
    const CREATED_TYPE_AUTO = 'auto';
    const CREATED_TYPE_MANUALLY = 'manually';

    const TYPE_XLS = 'xls';

    protected $table = 'reports';

    protected $fillable = ['start_date', 'end_date', 'type', 'path', 'object_id', 'created_type'];

    protected $dates = [
        'start_date',
        'end_date'
    ];

    protected $casts = [
        'out_of_range' => 'boolean'
    ];

    public function object()
    {
        return $this->belongsTo(ControlObject::class);
    }
}