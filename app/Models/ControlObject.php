<?php
/**
 * @author Dima Korets korets.web@gmail.com
 * @Date: 22.02.18
 */

namespace App\Models;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * Class ControlObject
 * @package App\Models
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property integer $min_value
 * @property integer $max_value
 * @property array $notification_channels
 * @property integer $frequency
 * @property integer $accident_chance
 * @property string $units
 * @property Carbon $last_read
 * @property boolean $active
 * @property Collection|null $results
 */
class ControlObject extends Model
{
    protected $table = 'control_objects';

    protected $fillable = [
        'name', 'description', 'min_value', 'max_value', 'units',
        'notification_channels', 'frequency', 'accident_chance', 'active'
    ];

    protected $dates = [
        'last_read'
    ];

    protected $casts = [
        'notification_channels' => 'array',
    ];

    public function results()
    {
        return $this->hasMany(Result::class, 'object_id');
    }

    public function scopeActive($query)
    {
        return $query->where('active', true);
    }

    public function activate()
    {
        $this->active = true;
        $this->save();

        return $this;
    }

    public function deactivate()
    {
        $this->active = false;
        $this->save();

        return $this;
    }

    /**
     * @return array
     */
    public static function getUnitsMap()
    {
        return [
            'celsius' => '°C',
            'ohm' => 'Ом',
            'tesla' => 'Тл',
            'pascal' => 'Па',
            'm' => 'м.',
            'sm' => 'см.',
        ];
    }

    /**
     * @return array
     */
    public function read()
    {
        $out = true;
        $value = $this->generateValue();
        if ($this->min_value <= $value && $this->max_value >= $value) {
            $out = false;
        }

        $this->results()->save(new Result(['value' => $value, 'out_of_range' => $out]));
        $this->last_read = Carbon::now();
        $this->save();

        return ['value' => $value, 'out_of_range' => $out, 'units' => self::getUnitsMap()[$this->units]];
    }

    public function getUnitSymbolAttribute()
    {
        return self::getUnitsMap()[$this->units];
    }

    /**
     * @return integer
     */
    public function generateValue()
    {
        $accidentChance = rand(0, 100);
        if ($this->accident_chance >= $accidentChance) {
            $lessOrBigger = rand(0, 1);
            if ($lessOrBigger) {
                return rand($this->max_value + 1, $this->max_value * 2);
            } else {
                if ($this->min_value > 0) {
                    return rand(0, $this->min_value);
                } elseif ($this->min_value < 0) {
                    return rand($this->min_value * 2, $this->min_value);
                } else {
                    return 0;
                }
            }
        }
        return rand($this->min_value, $this->max_value);
    }

    /**
     * @return int
     */
    public function getTotalReads()
    {
        return $this->results ? 0 : $this->results->count();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reports()
    {
        return $this->hasMany(Report::class);
    }

    /**
     * @return bool
     */
    public function hasChannel(string $channel)
    {
        return in_array($channel, $this->notification_channels);
    }
}