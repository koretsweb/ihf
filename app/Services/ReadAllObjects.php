<?php
/**
 * Created by PhpStorm.
 * User: pc60
 * Date: 23.02.18
 * Time: 13:52
 */

namespace App\Services;


use App\Models\ControlObject;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;

class ReadAllObjects
{
    protected $repository;

    public function __construct()
    {

    }

    public function run()
    {
        $values = [];
        $models = $this->getObjectsNeedToBeRead();

        if ($models->isNotEmpty()) {
            /**
             * @var $model ControlObject
             */
            foreach ($models as $model) {
                $values[$model->name] = $model->read();
            }
        }

        return $values;
    }

    /**
     * Get active objects that need to be read now(time comparing)
     * @return Collection|null
     */
    protected function getObjectsNeedToBeRead()
    {
        /**
         * @var Collection
         */
        $models = ControlObject::active()->get();

        $current = Carbon::now();

        /**
         * @var $model ControlObject
         */
        foreach ($models as $key => $model) {
            if ($model->last_read && $model->last_read->addSecond($model->frequency) > $current) {
                $models->forget($key);
            }
        }

        return $models;
    }
}