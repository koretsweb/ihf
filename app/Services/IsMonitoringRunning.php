<?php
/**
 * @author Dima Korets korets.web@gmail.com
 * @Date: 24.02.18
 */

namespace App\Services;


use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class IsMonitoringRunning
{
    /**
     * @return true
     */
    public static function run()
    {
        $process = new Process('ps -aux | grep artisan');
        $process->run();

        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        $response = $process->getIncrementalOutput();

        return str_contains($response, 'artisan runMonitor');
    }
}