<?php
/**
 * Created by PhpStorm.
 * User: pc60
 * Date: 26.02.18
 * Time: 9:30
 */

namespace App\Services;


use App\Models\ControlObject;
use App\Models\Result;
use Illuminate\Database\Eloquent\Collection;

class GenerateReport
{
    /**
     * @var ControlObject
     */
    protected $object;

    /**
     * @var string
     */
    protected $start;

    /**
     * @var string
     */
    protected $end;

    public function __construct(ControlObject $controlObject, string $start, string $end)
    {
        $this->object = $controlObject;
        $this->start = $start;
        $this->end = $end;
    }

    /**
     * @return Collection
     */
    public function getData()
    {
        $results = Result::where('object_id', $this->object->id)
            ->where('created_at', '>=', $this->start)
            ->where('created_at', '<=', $this->end)
            ->get();

        return $results;
    }
}