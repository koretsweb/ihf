<?php
/**
 * Created by PhpStorm.
 * User: pc60
 * Date: 23.02.18
 * Time: 16:53
 */

namespace App\Services;
use Twilio\Rest\Client;

/**
 * Class SendSMS has only one public method run(). It send and SMS message to the passed number via Twilio API
 * @package App\Services
 */
class SendSMS
{
    /**
     * @var string
     */
    protected $number;

    /**
     * @var string
     */
    protected $message;

    /**
     * @var string
     */
    protected $accountSid;

    /**
     * @var string
     */
    protected $token;

    /**
     * @var string
     */
    protected $twilioNumber;

    public function __construct(string $number, string $message)
    {
        $this->number = $number;
        $this->message = $message;

        $this->accountSid = env('TWILIO_SID');
        $this->token = env('TWILIO_TOKEN');
        $this->twilioNumber = env('TWILIO_NUMBER');
    }

    /**
     * @throws \Twilio\Exceptions\ConfigurationException
     */
    public function run()
    {
        $client = new Client($this->accountSid, $this->token);
        $message = $client->messages->create(
            $this->number,
            array(
                'from' => $this->twilioNumber,
                'body' => $this->message
            )
        );

        \Log::info(json_encode($message));
    }
}