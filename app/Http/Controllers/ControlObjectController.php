<?php

namespace App\Http\Controllers;

use App\Http\Requests\ControlObject\StoreControlObjectRequest;
use App\Http\Requests\ControlObject\UpdateControlObjectRequest;
use App\Models\ControlObject;
use App\Models\Result;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class ControlObjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /**
         * @var Collection
         */
        $models = ControlObject::with('results')
            ->orderBy('created_at', 'desc')
            ->get();

        return view('object.index', compact('models'));
    }

    public function fetch($id)
    {
        $model = ControlObject::find($id);
        $results = $model->results()->orderBy('created_at', 'desc')->take(1)->get();

        $response = [];

        foreach ($results as $result) {
            $response[] = [$result->id, $result->value];
        }

        return $response;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('object.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreControlObjectRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreControlObjectRequest $request)
    {
        ControlObject::create($request->all());

        return redirect()->route('object.index')->with('flash_success', "Об'єкт керування був успішно доданий");
    }

    /**
     * Display the specified resource.
     *
     * @param integer $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = ControlObject::find($id);

        return view('object.show', compact('model'));
    }

    /**
     * @param $id
     * @return string
     * @throws \Throwable
     */
    public function renderTable($id)
    {
        $model = ControlObject::find($id);

        return view('object.result_rows', compact('model'))->render();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param integer $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = ControlObject::find($id);

        return view('object.edit', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateControlObjectRequest $request
     * @param  integer $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateControlObjectRequest $request, $id)
    {
        $model = ControlObject::find($id);
        $model->fill($request->all())->save();

        return redirect(route('object.index'))
            ->with('flash_success', $model->name . ' був успішно оновлений');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @throws \Exception
     * @param  ControlObject  $controlObject
     * @return \Illuminate\Http\Response
     */
    public function destroy(ControlObject $controlObject)
    {
        $controlObject->delete();

        return back()->with('flash_success', $controlObject->name . ' був успішно видалений');
    }

    public function activate(ControlObject $controlObject)
    {
        $controlObject->activate();

        return back()->with('flash_success', $controlObject->name . ' був активований');
    }

    public function deactivate(ControlObject $controlObject)
    {
        $controlObject->deactivate();

        return back()->with('flash_success', $controlObject->name . ' був деактивований');
    }

    public function search()
    {
        $text = request()->input('text');
        $models = ControlObject::where('name', 'like', "%$text%")->with('results')->get();
        return view('object.index', compact('models'));
    }
}
