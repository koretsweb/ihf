<?php
/**
 * @author Dima Korets korets.web@gmail.com
 * @Date: 25.02.18
 */

namespace App\Http\Controllers;


use Illuminate\Database\Eloquent\Collection;

class NotificationsController extends Controller
{
    public function index()
    {
        /**
         * @var Collection $nots
         */
        $nots = \Auth::user()->unreadNotifications;

        foreach ($nots as $not) {
            $not->markAsRead();
        }

        $models = \Auth::user()->notifications->sortByDesc('created_at');

        return view('notifications.index', compact('models'));
    }
}