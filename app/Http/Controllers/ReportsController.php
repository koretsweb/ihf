<?php
/**
 * Created by PhpStorm.
 * User: pc60
 * Date: 26.02.18
 * Time: 9:19
 */

namespace App\Http\Controllers;


use App\Models\ControlObject;
use App\Models\Report;
use App\Services\GenerateReport;
use Illuminate\Http\Request;

class ReportsController extends Controller
{
    public function index()
    {
        $models = Report::all();
        $objects = ControlObject::all();

        return view('reports.index', compact('models', 'objects'));
    }

    public function download($id)
    {
        $model = Report::findOrFail($id);
    }

    public function generateAndDownload(Request $request)
    {
        $object = ControlObject::findOrFail($request->input('id'));

        $service = new GenerateReport($object, $request->input('start'), $request->input('end'));
        $data = $service->getData();
        $columns = $this->getColumnNames();

        $this->setHeaders('export.' . date('m.d.y') . '.xls');

        Report::create([
            'path' => 'default',
            'created_type' => Report::CREATED_TYPE_MANUALLY,
            'object_id' => $object->id,
            'start_date' => $request->input('start'),
            'end_date' => $request->input('end')
        ]);

        return view('reports.generated', compact('columns', 'data'));
    }

    protected function setHeaders($filename)
    {
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private", false);
        header("Content-Type: application/x-msexcel");
        header("Content-Disposition: attachment; filename=\"" . iconv('UTF-8', 'CP1251', $filename) . "\";");
        header("Content-Transfer-Encoding:­ binary");
    }

    /**
     * @return array of column names
     */
    public function getColumnNames()
    {
        return [
//            'Control Object',
            'Value',
            'Units',
            'Created at',
            'Is out of range',
        ];
    }

}