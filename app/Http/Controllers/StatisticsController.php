<?php
/**
 * @author Dima Korets korets.web@gmail.com
 * @Date: 25.02.18
 */

namespace App\Http\Controllers;


use App\Models\ControlObject;
use Illuminate\Http\Request;

class StatisticsController extends Controller
{
    public function index()
    {
        $objects = ControlObject::all();
        return view('statistics.index', compact('objects'));
    }

    public function process(Request $request)
    {
        $model = ControlObject::find($request->input('id'));

        $data = $model->results
            ->where('created_at', '>=', $request->input('from'))
            ->where('created_at', '<=', $request->input('to'))
            ->toArray();

        return back()
            ->with('data', json_encode($data))
            ->with('from', $request->input('from'))
            ->with('to', $request->input('to'));
    }
}