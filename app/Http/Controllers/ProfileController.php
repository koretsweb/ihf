<?php
/**
 * @author Dima Korets korets.web@gmail.com
 * @Date: 25.02.18
 */

namespace App\Http\Controllers;


use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function index()
    {
        $model = \Auth::user();

        return view('profile.index', compact('model'));
    }

    public function update(Request $request)
    {
        $model = \Auth::user();
        $model->fill($request->all())->save();


        return redirect()->route('profile.index')->with('flash_success', 'Профіль був оновлений');
    }

    public function edit()
    {
        return view('profile.edit', ['model' => \Auth::user()]);
    }
}