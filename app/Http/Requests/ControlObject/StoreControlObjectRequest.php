<?php
/**
 * @author Dima Korets korets.web@gmail.com
 * @Date: 22.02.18
 */

namespace App\Http\Requests\ControlObject;


use App\Rules\MaxMinValues;
use Illuminate\Foundation\Http\FormRequest;

class StoreControlObjectRequest extends FormRequest
{
    public function rules()
    {
        $this->request->add(['active' => $this->request->has('active')]);

        return [
            'name' => 'required|string',
            'description' => 'string',
            'min_value' => ['required', 'integer', new MaxMinValues()],
            'max_value' => 'required|integer',
            'units' => 'required|string',
            'frequency' => 'required|integer|between:5,100',
            'accident_chance' => 'required|integer|between:0,100',
            'notification_channels' => 'required|array',
        ];
    }

    public function attributes()
    {
        return [
            'name' => "Ім'я",
            'description' => 'Опис',
            'min_value' => 'Мінімальне значення',
            'max_value' => 'Максимальне значення',
            'units' => 'Одиниці вимірювання',
            'frequency' => 'Періодичність замірів',
            'accident_chance' => 'Ймовірність спрацювання сигналізації',
            'notification_channels' => 'Канали оповіщень',
        ];
    }

    public function authorize()
    {
        return \Auth::check();
    }
}