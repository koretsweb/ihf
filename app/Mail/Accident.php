<?php

namespace App\Mail;

use App\Models\Result;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Accident extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var Result
     */
    protected $result;

    /**
     * Create a new message instance.
     * @param Result $result
     * @return void
     */
    public function __construct(Result $result)
    {
        $this->result = $result;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.accident');
    }
}
