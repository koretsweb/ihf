@extends('layouts.app')
@section('content')
    <script type="text/javascript" src="{{ asset('js/moments.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap-datetimepicker.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('css/bootstrap-datetimepicker.min.css') }}" />

    <div class="container">
        <form action="{{ route('statistics.process') }}" method="post">
            {{ csrf_field() }}
            <div class="row">
                    <div class="col-sm-3">
                        <h3>Об'єкт керування</h3>
                        <select name="id" class="form-control" required>
                            @foreach($objects as $object)
                                <option value="{{$object->id}}">{{ $object->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class='col-sm-3'>
                        <h3>Початкова дата</h3>
                        <div class="form-group">
                            <div class='input-group date' id='from'>
                                <input type='text' name="from" required @if(session()->has('from')) value="{{ session()->get('from') }}" @endif class="form-control" />
                                <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                            </div>
                        </div>
                    </div>

                    <div class='col-sm-3'>
                        <h3>Кінцева дата</h3>
                        <div class="form-group">
                            <div class='input-group date' id='to'>
                                <input type='text' name="to" @if(session()->has('to')) value="{{ session()->get('to') }}" @endif required class="form-control" />
                                <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                            </div>
                        </div>
                    </div>
                <script type="text/javascript">
                    $(function () {
                        $('#from').datetimepicker({
                            format: 'YYYY-MM-DD HH:mm:ss'
                        });
                        $('#to').datetimepicker({
                            format: 'YYYY-MM-DD HH:mm:ss'
                        });
                    });
                </script>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <button type="submit" class="btn btn-primary">Побудувати графік</button>
                </div>
            </div>
        </form>
        @if(session()->has('data'))
        <div class="row" style="margin-top: 40px;">
            <div id="graph" class="col-sm-12">
                <div class="panel-body">
                    <div id="models" data="{{ session()->get('data') }}"></div>
                    <div id="demo-line-chart" class="ct-chart"></div>
                </div>
            </div>
            <script>
                $(function () {
                    var models = JSON.parse($('#models').attr("data"));

                    var labels = [];
                    var series = [];

                    models.forEach(function(element) {
                        series.push(element.value);
                        labels.push(element.created_at);
                    });

                    console.log(labels);
                    console.log(series);

                    var data = {
                        labels: labels,
                        series: [series]
                    };
                    options = {
                        height: "300px",
                        showPoint: true,
                        axisX: {
                            showGrid: false
                        },
                        lineSmooth: false,
                    };

                    new Chartist.Line('#demo-line-chart', data, options);
                })
            </script>
        </div>
        @endif
    </div>
@endsection