@extends('layouts.app')

@section('content')
    <script type="text/javascript" src="{{ asset('js/moments.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap-datetimepicker.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('css/bootstrap-datetimepicker.min.css') }}" />

    <h3 class="page-title">Звіти</h3>
    <div class="row">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Найновіші зверху</h3>
            </div>
            <div class="panel-body">
                @include('reports.generate')
                @if($models->isEmpty())
                    Немає записів
                @else
                    @include('reports.table')
                @endif
            </div>
        </div>
    </div>

@endsection