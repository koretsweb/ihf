<HTML xmlns:v="urn:schemas-microsoft-com:vml"
      xmlns:o="urn:schemas-microsoft-com:office:office"
      xmlns:x="urn:schemas-microsoft-com:office:excel"
      xmlns="http://www.w3.org/TR/REC-html40">
<HEAD>
    <meta http-equiv=Content-Type content="text/html; charset=windows-1251">
    <STYLE>
        table {
            mso-displayed-decimal-separator:"\,";
            mso-displayed-thousand-separator:" ";
        }
        td {
            text-decoration: none;
            font-family: Tahoma, Arial;
            border:none;
            white-space:nowrap;
        }
        b { font-weight: normal }   </STYLE>
    <!--[if gte mso 9]><xml>
        <x:ExcelWorkbook>
            <x:ExcelWorksheets>
                <x:ExcelWorksheet>
                    <x:Name>export</x:Name>
                    <x:WorksheetOptions>
                        <x:NoSummaryRowsBelowDetail/>
                        <x:Print>
                            <x:ValidPrinterInfo/>
                            <x:PaperSizeIndex>9</x:PaperSizeIndex>
                            <x:VerticalResolution>0</x:VerticalResolution>
                        </x:Print>
                        <x:Zoom>20</x:Zoom>
                        <x:Selected/>
                        <x:FreezePanes/>
                        <x:FrozenNoSplit/>
                        <x:SplitHorizontal>2</x:SplitHorizontal>
                        <x:TopRowBottomPane>2</x:TopRowBottomPane>
                        <x:SplitVertical>100</x:SplitVertical>
                        <x:LeftColumnRightPane>4</x:LeftColumnRightPane>
                        <x:ActivePane>0</x:ActivePane>
                        <x:Panes>
                            <x:Pane>
                                <x:Number>3</x:Number>
                            </x:Pane>
                            <x:Pane>
                                <x:Number>1</x:Number>
                                <x:ActiveCol>6</x:ActiveCol>
                            </x:Pane>
                            <x:Pane>
                                <x:Number>2</x:Number>
                                <x:ActiveRow>2</x:ActiveRow>
                            </x:Pane>
                            <x:Pane>
                                <x:Number>0</x:Number>
                                <x:ActiveCol>9</x:ActiveCol>
                            </x:Pane>
                        </x:Panes>
                        <x:ProtectContents>False</x:ProtectContents>
                        <x:ProtectObjects>False</x:ProtectObjects>
                        <x:ProtectScenarios>False</x:ProtectScenarios>
                    </x:WorksheetOptions>
                </x:ExcelWorksheet>
            </x:ExcelWorksheets>
            <x:WindowHeight>7425</x:WindowHeight>
            <x:WindowWidth>14235</x:WindowWidth>
            <x:WindowTopX>480</x:WindowTopX>
            <x:WindowTopY>30</x:WindowTopY>
            <x:ProtectStructure>False</x:ProtectStructure>
            <x:ProtectWindows>False</x:ProtectWindows>
        </x:ExcelWorkbook>
    </xml><![endif]--><!--[if gte mso 9]><xml>
        <o:shapedefaults v:ext="edit" spidmax="1030"/>
    </xml><![endif]--></HEAD>
<body>

<table border="1">
    <tr>
        {{--draw column names row--}}
        @foreach($columns as $column)
        <td>{{ $column }}</td>
        @endforeach
    </tr>
    {{-- draw content rows--}}
    @foreach($data as $item)
    <tr>
{{--        <td>{{ utf8_decode($item->object->name) }}</td>--}}
        <td>{{ $item->value }}</td>
        <td>{{\App\Models\ControlObject::getUnitsMap()[$item->object->units]}}</td>
        <td>{{$item->created_at}}</td>
        <td>{{$item->out_of_range ? '+' : '-'}}</td>
    </tr>
    @endforeach
</table>

</body>
</html>