<form action="{{ route('reports.generate') }}" method="post">
    {{ csrf_field() }}
    <div class="row">
        <div class="col-sm-3">
            <h3>Об'єкт керування</h3>
            <select name="id" class="form-control" required>
                @foreach($objects as $object)
                    <option value="{{$object->id}}">{{ $object->name }}</option>
                @endforeach
            </select>
        </div>
        <div class='col-sm-3'>
            <h3>Початкова дата</h3>
            <div class="form-group">
                <div class='input-group date' id='start'>
                    <input type='text' name="start" required class="form-control" />
                    <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                </div>
            </div>
        </div>

        <div class='col-sm-3'>
            <h3>Кінцева дата</h3>
            <div class="form-group">
                <div class='input-group date' id='end'>
                    <input type='text' name="end" required class="form-control" />
                    <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $(function () {
                $('#start').datetimepicker({
                    format: 'YYYY-MM-DD HH:mm:ss'
                });
                $('#end').datetimepicker({
                    format: 'YYYY-MM-DD HH:mm:ss'
                });
            });
        </script>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <button type="submit" class="btn btn-primary">Згенерувати та завантажити</button>
        </div>
    </div>
</form>
