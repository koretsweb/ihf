<table class="table table-hover">
    <thead>
    <tr>
        <th>#</th>
        <th>Об'єкт</th>
        <th>Тип файлу</th>
        <th>Сгенеровано</th>
        <th>Початковий час</th>
        <th>Кінцевий час</th>
        <th>Шлях</th>
        <th>Режим створення</th>
        <th>Діі</th>
    </tr>
    </thead>
    <tbody>
    @foreach($models as $id => $model)
        <tr>
            <td>{{ ++$loop->index }}</td>
            <td><a href="{{ route('object.edit', $model->object->id) }}">{{ $model->object->name }}</a></td>
            <td>{{ $model->type }}</td>
            <td>{{ $model->created_at }}</td>
            <td>{{ $model->start_date }}</td>
            <td>{{ $model->end_date }}</td>
            <td>{{ $model->path }}</td>
            <td>{{ $model->created_type === \App\Models\Report::CREATED_TYPE_MANUALLY ? 'Ручний' : 'Автоматичний' }}</td>
            <td>
                <i class="fa fa-download"><a href="{{ route('reports.download', $model->id) }}">Завантажити</a></i>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>