@extends('layouts.app')

@section('content')

    <h3 class="page-title">{{ $model->name }}</h3>
    <div class="row">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title"><b>Опис:</b> {{ $model->description }}</h3>
            </div>
            <div class="panel-body">
                <h3>Результати</h3>
                @if($model->results->isEmpty())
                    Поки не було зроблено жодного зняття показників
                @else
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Значення ({{\App\Models\ControlObject::getUnitsMap()[$model->units]}})</th>
                            <th>Дата</th>
                        </tr>
                        </thead>
                        <tbody id="rows">
                            @include('object.result_rows')
                        </tbody>
                    </table>
                @endif
            </div>
            <div id="field" class="hidden" data-field-id="{{$model->id}}" ></div>
        </div>
    </div>
    <script>
        var fieldId = $('#field').data("field-id");
        function sleep (time) {
            return new Promise((resolve) => setTimeout(resolve, time));
        }
        function render() {

            $.ajax(
                {
                    url: "/object/" + fieldId + "/render-table",
                    success: function(result){
                        $("#rows").html(result);
                    }
                });

            sleep(1000).then(() => {
                render();
            });

        }

        render();
    </script>

@endsection