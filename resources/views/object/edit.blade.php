@extends('layouts.app')

@section('content')


    <h3 class="page-title">Редагувати {{ $model->name }}</h3>
    <div class="row">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Заповніть та підтвердіть форму нижче</h3>
            </div>
            <div class="panel-body">
                <form action="{{ route('object.update', $model->id) }}" method="post">

                    @include('helper.errors')

                    <div class="col-md-6">
                        {{ csrf_field() }}
                        {{ method_field('put') }}
                        <label for="name">Ім'я*</label>
                        <input type="text" name="name" value="{{ $model->name }}" required class="form-control">
                        <br>
                        <label for="description">Опис</label>
                        <input type="text" name="description" value="{{ $model->description ? $model->description : '' }}" class="form-control">
                        <br>
                        <label for="units">Одиниці виміру
                            <select name="units" class="form-control" required>
                                <option value="{{ $model->units }}" selected>{{ \App\Models\ControlObject::getUnitsMap()[$model->units] }}</option>
                                @foreach(\App\Models\ControlObject::getUnitsMap() as $value => $text)
                                    @if ($model->units != $value)
                                        <option value="{{$value}}">{{ $text }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </label>
                        <br>
                        <label for="min_value">Мінімальне значення</label>
                        <input type="text" value="{{ $model->min_value }}" name="min_value" class="form-control" required>
                        <br>
                        <label for="max_value">Максимальне значення</label>
                        <input type="text" name="max_value" value="{{ $model->max_value }}" class="form-control" required>
                        <br>
                        <label for="active">Статус</label>
                        <label class="fancy-checkbox">
                            <input name="active" type="checkbox" {{ $model->active ? 'checked' : '' }}>
                            <span>Активний</span>
                        </label>
                    </div>
                    <div class="col-md-6">
                        <label for="frequency">Періодичність замірів(в секундах)</label>
                        <input type="text" name="frequency" value="{{ $model->frequency }}" class="form-control" required>
                        <br>
                        <label for="accident_chance">Ймовірність спрацювання сигналізації(у %)</label>
                        <input type="text" name="accident_chance" value="{{ $model->accident_chance }}" class="form-control" required>
                        <br>
                        <label for="notification_channels[]">Канали оповіщень</label>
                        <label class="fancy-checkbox">
                            <input type="checkbox" @if($model->hasChannel('\NotificationChannels\Telegram\TelegramChannel')) checked @endif  value="\NotificationChannels\Telegram\TelegramChannel" name="notification_channels[]">
                            <span>Telegram</span>
                        </label>
                        <label class="fancy-checkbox">
                            <input type="checkbox" value="mail" @if($model->hasChannel('mail')) checked @endif name="notification_channels[]">
                            <span>Email</span>
                        </label>
                        <label class="fancy-checkbox">
                            <input type="checkbox" value="App\Notifications\Drivers\SmsChannel" @if($model->hasChannel('App\Notifications\Drivers\SmsChannel')) checked @endif name="notification_channels[]">
                            <span>SMS</span>
                        </label>
                        <br>
                        <button type="submit" class="btn btn-success">Оновити</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


@endsection