<script>
    window.onload = function() {

        var dataPoints = [];

        var chart = new CanvasJS.Chart("chartContainer{{$model->id}}", {
            theme: "light2",
            title: {
                text: "{{ $model->name . "( $model->unitSymbol )" }}"
            },
            data: [{
                type: "line",
                dataPoints: dataPoints
            }]
        });
        updateData();

// Initial Values
        var xValue = 0;
        var yValue = 10;
        var newDataCount = 6;

        function addData(data) {
            if(newDataCount != 1) {
                $.each(data, function(key, value) {

                    dataPoints.push({x: value[0], y: parseInt(value[1])});
                    xValue++;
                    yValue = parseInt(value[1]);
                });
            } else {
                //dataPoints.shift();
                dataPoints.push({x: data[0][0], y: parseInt(data[0][1])});
                xValue++;
                yValue = parseInt(data[0][1]);
            }

            newDataCount = 1;
            chart.render();
            setTimeout(updateData, 1500);
        }

        function updateData() {
            $.getJSON("/fetch-object/{{$model->id}}", addData);
            // $.getJSON("https://canvasjs.com/services/data/datapoints.php?xstart="+xValue+"&ystart="+yValue+"&length="+newDataCount+"type=json", addData);
        }

    }
</script>
<div id="chartContainer{{$model->id}}" style="height: 370px; max-width: 920px; margin: 0px auto;"></div>