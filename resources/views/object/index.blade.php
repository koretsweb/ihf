@extends('layouts.app')

@section('content')

    <h3 class="page-title">Об'єкти керувань</h3>
    <div class="row">
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">В порядку спадання дати добавлення</h3>
                </div>
                <div class="panel-body">
                    <p>
                        <a href="{{ route('object.create') }}">
                            <button type="button" class="btn btn-primary right">Додати</button>
                        </a>
                    </p>
                    @if($models->isEmpty())
                        Немає записів
                    @else
                        @include('object.table')
                    @endif
                </div>
            </div>
    </div>

@endsection