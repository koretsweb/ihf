@extends('layouts.app')

@section('content')


    <h3 class="page-title">Додати новий об'єкт керування</h3>
    <div class="row">
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">Заповніть та підтвердіть форму нижче</h3>
                </div>
                <div class="panel-body">
                    <form action="{{ route('object.store') }}" method="post">

                        @include('helper.errors')

                        <div class="col-md-6">
                                {{ csrf_field() }}
                                <label for="name">Ім'я*</label>
                                <input type="text" name="name" value="{{ old('name') }}" placeholder="теплообмінник" required class="form-control">
                                <br>
                                <label for="description">Опис</label>
                                <input type="text" name="description" value="{{ old('description') }}" class="form-control" placeholder="короткий опис">
                                <br>
                                <label for="units">Одиниці виміру
                                    <select name="units" class="form-control" required>
                                        @foreach(\App\Models\ControlObject::getUnitsMap() as $value => $text)
                                                <option value="{{$value}}">{{ $text }}</option>
                                        @endforeach
                                    </select>
                                </label>
                                <br>
                                <label for="min_value">Мінімальне значення</label>
                                <input type="text" value="{{ old('min_value') }}" name="min_value" class="form-control" required placeholder="20">
                                <br>
                                <label for="max_value">Максимальне значення</label>
                                <input type="text" name="max_value" value="{{ old('max_value') }}" class="form-control" required placeholder="140">
                                <br>
                                <label for="active">Статус</label>
                                <label class="fancy-checkbox">
                                    <input name="active" type="checkbox" checked>
                                    <span>Активний</span>
                                </label>
                        </div>
                        <div class="col-md-6">
                            <label for="frequency">Періодичність замірів(в секундах)</label>
                            <input type="text" name="frequency" value="{{ old('frequency') }}" class="form-control" required>
                            <br>
                            <label for="accident_chance">Ймовірність спрацювання сигналізації(у %)</label>
                            <input type="text" name="accident_chance" value="{{ old('accident_chance') }}" class="form-control" required>
                            <br>
                            <label for="notification_channels[]">Канали оповіщень</label>
                            <label class="fancy-checkbox">
                                <input type="checkbox" value="\NotificationChannels\Telegram\TelegramChannel" name="notification_channels[]">
                                <span>Telegram</span>
                            </label>
                            <label class="fancy-checkbox">
                                <input type="checkbox" value="mail" checked name="notification_channels[]">
                                <span>Email</span>
                            </label>
                            <label class="fancy-checkbox">
                                <input type="checkbox" value="App\Notifications\Drivers\SmsChannel" name="notification_channels[]">
                                <span>SMS</span>
                            </label>
                            <br>
                            <button type="submit" class="btn btn-success">Зберегти</button>
                        </div>
                    </form>
                </div>
            </div>
    </div>


@endsection