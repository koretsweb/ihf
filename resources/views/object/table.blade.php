<table class="table table-hover">
    <thead>
    <tr>
        <th>#</th>
        <th>Назва</th>
        <th>Опис</th>
        <th>Зроблено замірів</th>
        <th>Аварійних ситуацій</th>
        <th>Періодичність моніторингу</th>
        <th>Активний</th>
        <th width="125px">Дії</th>
    </tr>
    </thead>
    <tbody>
    @foreach($models as $id => $model)
        <tr class="{{ $model->active ?: 'warning' }}">
            <td>{{++$id}}</td>
            <td>{{$model->name}}</td>
            <td>{{$model->description}}</td>
            <td>{{$model->results()->count()}}</td>
            <td>{{$model->results()->outOfRange()->count()}}</td>
            <td>{{$model->frequency}} с.</td>
            <td>{{$model->active ? 'Так' : 'Ні'}}</td>
            <td>
                @if ($model->active)
                    <a href="{{ route('object.deactivate', $model->id) }}" title="Деактивувати">
                        <i class="fa fa-fw fa-check"></i>
                    </a>
                @else
                    <a href="{{ route('object.activate', $model->id) }}" title="Активувати">
                        <i class="fa fa-fw fa-check"></i>
                    </a>
                @endif
                <a href="{{ route('object.show', $model->id) }}" title="Детальніше">
                    <i class="fa fa-fw fa-eye"></i>
                </a>
                <a href="{{ route('object.edit', $model->id) }}" title="Редагувати">
                    <i class="fa fa-fw fa-edit"></i>
                </a>
                <a id="delete_object" href="{{ route('object.delete_get', $model->id) }}" title="Видалити">
                    <i class="fa fa-fw fa-trash"></i>
                </a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
<script>
    $("#delete_object").click(function (e) {
        e.preventDefault();
        if (confirm("Ви впевнені, що хочете видалити цей об'єкт керування? Також будуть видалені всі записи про минулі заміри")) {
            window.location.href = $(this).attr('href');
        }
    })
</script>