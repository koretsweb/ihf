@foreach($model->results->sortByDesc('created_at') as $result)
    <tr class="{{ $result->out_of_range ? 'danger' : ''  }}">
        <td>{{++$loop->index}}</td>
        <td>{{$result->value}}</td>
        <td>{{$result->created_at}}</td>
    </tr>
@endforeach