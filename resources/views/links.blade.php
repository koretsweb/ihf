@extends('layouts.app')
@section('content')
    <h3>Технологічний стек</h3>
    <p>
        У якості бекенд частини використовується:
    <ul>
        <li>
            php 7.1
        </li>
        <li>Laravel 5.5</li>
        <li>Twilio (package)</li>
    </ul>
    </p>
    <p>
        Фронтенд бібліотеки:
    <ul>
        <li>Bootstrap</li>
        <li>jQuery</li>
        <li>Datepicker</li>
        <li>Moments</li>
    </ul>
    </p>
@endsection