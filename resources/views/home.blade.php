@extends('layouts.app')

@section('content')

    <!-- OVERVIEW -->
    <div class="panel panel-headline">
        <div class="panel-heading">
            <h3 class="panel-title">Огляд</h3>
            <p class="panel-subtitle">Загальна інформація</p>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-3">
                    <div class="metric">
                        <span class="icon"><i class="fa fa-eye"></i></span>
                        <p>
                            <span class="number">{{ \App\Services\IsMonitoringRunning::run() ? 'Так' : 'Ні' }}</span>
                            <span class="title">Моніторинг увімкнений</span>
                        </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="metric">
                        <span class="icon"><i class="fa fa-exclamation-triangle"></i></span>
                        <p>
                            <span class="number">{{ \App\Models\Result::outOfRange()->count() }}</span>
                            <span class="title">Спрацювань сигналізації</span>
                        </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="metric">
                        <span class="icon"><i class="fa fa-download"></i></span>
                        <p>
                            <span class="number">{{ \App\Models\Result::count() }}</span>
                            <span class="title">Зроблено замірів</span>
                        </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="metric">
                        <span class="icon"><i class="fa fa-bar-chart"></i></span>
                        <p>
                            <span class="number">{{ \App\Models\Report::count() }}</span>
                            <span class="title">Сгенеровано звітів</span>
                        </p>
                    </div>
                </div>
            </div>
            @php $models = \App\Models\ControlObject::all() @endphp
            @foreach($models as $model)
                <div>
                    @include('object.chart', ['model' => $model])
                </div>
            @endforeach
            <br>
            <div>
                <h3><b>Cхема процесу отримання сірчатого газу з відпрацьованої сірчаної кислоти</b></h3>
                <img src="{{ asset('img/schema.png') }}" class="img-responsive" alt="">
            </div>
        </div>
    </div>
    <!-- END OVERVIEW -->
@endsection
