@extends('layouts.app')

@section('content')


    <h3 class="page-title">Додати новий об'єкт керування</h3>
    <div class="row">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Заповніть та підтвердіть форму нижче</h3>
            </div>
            <div class="panel-body">
                <form action="{{ route('profile.update') }}" method="post">

                    @include('helper.errors')

                    <div class="col-md-6">
                        {{ csrf_field() }}
                        {{ method_field('put') }}
                        <label for="email">Email</label>
                        <input type="text" name="email" value="{{ Auth::user()->email }}" required class="form-control">
                        <br>
                        <label for="name">Name</label>
                        <input type="text" name="name" value="{{ Auth::user()->name }}" required class="form-control">
                        <br>
                        <label for="phone">Phone</label>
                        <input type="text" name="phone" value="{{ Auth::user()->phone }}" class="form-control" readonly>
                        <br>
                        <label for="min_value">Telegram</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-at"></i></span>
                            <input class="form-control" name="telegram" value="{{ Auth::user()->telegram }}" type="text">
                        </div>
                        <br>
                        <button type="submit" class="btn btn-primary">Зберегти</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


@endsection