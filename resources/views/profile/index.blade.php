@extends('layouts.app')
@section('content')

    <div class="panel panel-profile">
        <div class="clearfix">
            <!-- LEFT COLUMN -->
            <div class="profile-left">
                <!-- PROFILE HEADER -->
                <div class="profile-header">
                    <div class="overlay"></div>
                    <div class="profile-main">
                        <img src="{{ asset('img/ihf_logo.png') }}" class="img-circle img-responsive" alt="Avatar">
                        <h3 class="name">{{ $model->name }}</h3>
                        <span class="online-status status-available">Онлайн</span>
                    </div>
                    {{--<div class="profile-stat">--}}
                        {{--<div class="row">--}}
                            {{--<div class="col-md-4 stat-item">--}}
                                {{--45 <span>Projects</span>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-4 stat-item">--}}
                                {{--15 <span>Awards</span>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-4 stat-item">--}}
                                {{--2174 <span>Points</span>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                </div>
            </div>
            <div class="profile-right">
                <div class="profile-detail">
                    <div class="profile-info">
                        <h4 class="heading">Інформація</h4>
                        <ul class="list-unstyled list-justify">
                            <li>Email<span>{{ $model->email }}</span></li>
                            <li>Ім'я<span>{{ $model->name }}</span></li>
                            <li>Телефон<span>{{ $model->phone }}</span></li>
                            <li>Телеграм<span>{{ $model->telegram }}</span></li>
                            <li>Зареєстрований<span>{{ $model->created_at }}</span></li>
                            <li>Оновлений<span>{{ $model->updated_at }}</span></li>
                        </ul>
                    </div>
                    <div class="profile-info">
                        <h4 class="heading">Додаткова інформація</h4>
                        <p>Інженер першої категорії з максимальним рівнем доступу в системи</p>
                    </div>
                    <div class="text-center"><a href="{{ route('profile.edit') }}" class="btn btn-primary">Редагувати</a></div>
                </div>
            </div>
        </div>
    </div>

@endsection()