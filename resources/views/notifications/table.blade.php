<table class="table table-hover">
    <thead>
    <tr>
        <th>#</th>
        <th>Показники</th>
        <th>Об'єкт</th>
        <th>Створено</th>
    </tr>
    </thead>
    <tbody>
    @foreach($models as $id => $model)
        @php
            $result = \App\Models\Result::find($model->data['result_id'])
        @endphp
        <tr>
            <td>{{ ++$loop->index }}</td>
            <td>{{ $result->value . \App\Models\ControlObject::getUnitsMap()[$result->object->units] }}</td>
            <td><a href="{{ route('object.edit', $result->object->id) }}">{{ $result->object->name }}</a></td>
            <td>{{ $model->created_at }}</td>
        </tr>
    @endforeach
    </tbody>
</table>