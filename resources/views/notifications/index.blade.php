@extends('layouts.app')

@section('content')

    <h3 class="page-title">Оповіщення</h3>
    <div class="row">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Найновіші зверху</h3>
            </div>
            <div class="panel-body">
                @if($models->isEmpty())
                    Немає записів
                @else
                    @include('notifications.table')
                @endif
            </div>
        </div>
    </div>

@endsection