@extends('layouts.app')
@section('content')

    <h3 class="page-title">Про систему</h3>
    <div class="row">
        <div class="col-md-8">
            <!-- PANEL HEADLINE -->
            <div class="panel panel-headline">
                <div class="panel-heading">
                    <h3 class="panel-title">Система монітингу за об'єктами керувань</h3>
                </div>
                <div class="panel-body">
                    <p>
                        Вхідною точкою є додані в систему об'єкти керування з усіма необхідними налаштуваннями. Все залежить від них.
                        Після цього можна запускати моніторинг. Для цього необхідно виконати команду <code>php artisan runMonitor</code> з кореню проекта.
                        <br>
                        Не закриваючи термінал, можна в режимі реального часу спостерігати за емуляцією зняття показників з різноманітних датчиків.
                        <br>
                        Важливо зазначити, що скануються лише активні об'єкти
                        <hr>
                        У випадку, коли показники не входять у визначений раніше діапазон значень - спрацьовує <b>сигналізація</b>, яка виконана у вигляді безвідмовної системи оповіщень.
                        Наразі підтримуються такі канали:
                        <ul>
                            <li>Email</li>
                            <li>Telegram</li>
                            <li>SMS</li>
                            <li>Web</li>
                        </ul>
                    <b>Примітка:</b> Для оповіщення через telegram необхідно окрім налаштувань свого <code>@username</code> в профілі ще й під'єднатись до бота <code>@KpiIhfKoretsBot</code>
                    <hr>
                        Всі сгенеровані звіти зберігаються в локальному сховищі на тому ж сервері, де була розгорнута дана система.
                        Всі вони доступні в будь-який момент часу для завантаження на свою робочу машину, якщо є відповідні права доступу.
                    <br>
                        Наразі, автоматичні звіти генеруються у форматі .xls, в подальшому, при необхідності, можливо додати .xml, .csv, .json формати
                        та графічну репрезентацію в html аналогічно, як це виконано в розділі <i><a href="{{ route('statistics.index') }}">Статистика</a></i>.
                    </p>
                </div>
            </div>
            <!-- END PANEL HEADLINE -->
        </div>
        <div class="col-md-4">
            <!-- PANEL NO PADDING -->
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">Додатково</h3>
                    <div class="right">
                        <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                        <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                    </div>
                </div>
                <div class="panel-body no-padding bg-primary text-center">
                    <div class="padding-top-30 padding-bottom-30">
                        <i class="fa fa-address-card fa-5x"></i>
                        <h5>Помітили помилку або маєте пропозиції щодо покращення продукту?</h5>
                        <p><b>Email:</b> korets.web@gmail.com</p>
                    </div>
                </div>
            </div>
            <!-- END PANEL NO PADDING -->
        </div>
    </div>
@endsection