<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('logout', "Auth\LoginController@logout");
Route::get('home', function () {
    return redirect('/');
});
Route::group([
    'middleware' => ['web', 'auth']
], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::resource('object', 'ControlObjectController');
    Route::get('object/{control_object}/render-table', 'ControlObjectController@renderTable')->name('object.render_table');
    Route::get('object/{control_object}/activate', 'ControlObjectController@activate')->name('object.activate');
    Route::get('object/{control_object}/deactivate', 'ControlObjectController@deactivate')->name('object.deactivate');
    Route::get('object/{control_object}/delete', 'ControlObjectController@destroy')->name('object.delete_get');
    Route::get('object-search', 'ControlObjectController@search')->name('object.search');
    Route::get('fetch-object/{control_object}', 'ControlObjectController@fetch')->name('object.fetch');

    Route::get('profile', "ProfileController@index")->name('profile.index');
    Route::get('profile/edit', "ProfileController@edit")->name('profile.edit');
    Route::put('profile', "ProfileController@update")->name('profile.update');

    Route::get('notifications', 'NotificationsController@index')->name('notifications.index');

    Route::get('statistics', 'StatisticsController@index')->name('statistics.index');
    Route::post('statistics', 'StatisticsController@process')->name('statistics.process');

    Route::view('/info', 'info');
    Route::view('/links', 'links');

    Route::get('reports', 'ReportsController@index')->name('reports.index');
    Route::post('reports/generate', 'ReportsController@generateAndDownload')->name('reports.generate');
    Route::get('reports/{id}', 'ReportsController@download')->name('reports.download');
});